/**
 * This file is the main file for cordova
 * plugin, which gets called from JavaScript
 * source.
 *
 * Developed by Fuad Nazal.
 */
package com.bch;

import com.bch.RootUtils;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* Marked as errors in most common linters */
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.PluginResult;
import org.apache.cordova.PluginResult.Status;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaWebView;

public class RootDetector extends CordovaPlugin {

	private RootUtils rootUtils = new RootUtils();

	@Override
	public void initialize(CordovaInterface cordova, CordovaWebView webView) {
		super.initialize(cordova, webView);
	}

	@Override
	public boolean execute(String action, JSONArray array, CallbackContext callbackContext) throws JSONException {
		if (action.equals("listApps")) {
			callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK,
			    rootUtils.listApps(this.cordova.getActivity().getPackageManager()).toString()));
			return true;
		}
		else if (action.equals("lookForRootApp")) {
			JSONObject status = rootUtils.lookForIllegalApps(this.cordova.getActivity().getPackageManager());
			callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, status));
			return status.getBoolean("Detected");
		}
		else if (action.equals("isSELinux")) {
			JSONObject status = rootUtils.isSELinuxEnforced();
			callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, status));
			return status.getBoolean("Status");
		}
		else if (action.equals("isEmulatorOrActiveDebugging")) {
			JSONObject status = rootUtils.lookForDebugging(this.cordova.getActivity().getApplicationContext());
			callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, status.getBoolean("ActiveDebugging")));
			return status.getBoolean("ActiveDebugging");
		}
		else {
			callbackContext.error("Ha ocurrido un error");
			return false;
		}
	}
}
