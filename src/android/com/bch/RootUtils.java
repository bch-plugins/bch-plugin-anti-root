package com.bch;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.Process;
import java.util.ArrayList;
import java.util.List;

import java.io.InputStream;
import java.util.Scanner;
import java.io.File;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.provider.Settings;
import android.util.Log;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class RootUtils {
	public static final String[] knownRootAppsPackages = {
			"com.noshufou.android.su",
			"com.noshufou.android.su.elite",
			"eu.chainfire.supersu",
			"com.koushikdutta.superuser",
			"com.thirdparty.superuser",
			"com.yellowes.su",
			"com.zachspong.temprootremovejb",
			"com.ramdroid.appquarantine",
			"eu.chainfire.supersu",
			"com.topjohnwu.magisk",
			"com.elderdrivers.riru.edxp.yahfa",
			"com.elderdrivers.riru.edxp.whale",
			"com.elderdrivers.riru.edxp.sandhook",
			"com.elderdrivers.riru.edxp",
			"com.elderdrivers.riru.edxp.common",
			"eu.chainfire.libsuperuser",
			"com.kingroot.kinguser",
			"com.kingoapp.apk"
	};
	public static final String[] knownDangerousAppsPackages = {
			"com.koushikdutta.rommanager",
			"com.koushikdutta.rommanager.license",
			"com.dimonvideo.luckypatcher",
			"com.chelpus.lackypatch",
			"com.ramdroid.appquarantine",
			"com.ramdroid.appquarantinepro"
	};
	public static final String[] knownRootCloakingPackages = {
			"com.devadvance.rootcloak",
			"com.devadvance.rootcloakplus",
			"de.robv.android.xposed.installer",
			"com.saurik.substrate",
			"com.zachspong.temprootremovejb",
			"com.amphoras.hidemyroot",
			"com.amphoras.hidemyrootadfree",
			"com.formyhm.hiderootPremium",
			"com.formyhm.hideroot",
			"eu.chainfire.suhide"
	};
	private final String[] suPaths = {
    		"/data/local/su",
    		"/data/local/bin/su",
    		"/data/local/xbin/su",
    		"/sbin/su",
    		"/su/bin/su",
    		"/system/bin/su",
    		"/system/bin/.ext/su",
    		"/system/bin/failsafe/su",
    		"/system/sd/xbin/su",
    		"/system/usr/we-need-root/su",
    		"/system/xbin/su",
    		"/cache/su",
    		"/data/su",
    		"/dev/su",
    		"/sbin/.magisk"
    };
	private final String[] pathsThatShouldNotBeWritable = {
			"/system",
			"/system/bin",
			"/system/sbin",
			"/system/xbin",
			"/vendor/bin",
			"/sbin",
			"/etc"
	};
	private static final String seLinuxCommand = "getenforce";
	private static final String[] seLinuxPropCommand = { "getprop", "ro.boot.selinux" };
	private static final String[] seLinuxStatus = { "Enforcing", "Permissive", "enforcing", "permissive" };
	private static final String[] seLinuxPropStatus = { "enforcing", "permissive" };

	public JSONArray listApps(PackageManager packageManager) throws JSONException {
		return searchUserPackages(packageManager);
	}

	public JSONObject lookForIllegalApps(PackageManager packageManager) throws JSONException {
		return searchIllegalApps(packageManager);
	}

	public JSONObject isSELinuxEnforced() throws JSONException {
		JSONObject jsonObject = new JSONObject();
		StringBuilder seLinuxEnforceAppender = new StringBuilder();
		StringBuilder seLinuxPropAppender = new StringBuilder();
		Process processEnforce;
		Process processProp;
		String bufferEnforce;
		String bufferProp;
		String seLinuxEnforceResponseValue;
		String seLinuxPropResponseValue;

		try {
			processEnforce = Runtime.getRuntime().exec(seLinuxCommand);
			processProp = Runtime.getRuntime().exec(seLinuxPropCommand);
			processEnforce.waitFor();
			processProp.waitFor();
			BufferedReader bufferProcEnforce = new BufferedReader(new InputStreamReader(processEnforce.getInputStream()));
			BufferedReader bufferProcProp = new BufferedReader(new InputStreamReader(processProp.getInputStream()));
			while ((bufferEnforce = bufferProcEnforce.readLine()) != null) {
				seLinuxEnforceAppender.append(bufferEnforce);
			}
			while ((bufferProp = bufferProcProp.readLine()) != null) {
			    seLinuxPropAppender.append(bufferProp);
			}
		}
		catch (IOException e) {
		    Log.e("MiPass", e.getMessage(), e);
			jsonObject.put("Error", e.getMessage());
			jsonObject.put("Result", false);
			return jsonObject;
		}
		catch (InterruptedException e) {
			Log.e("MiPass", e.getMessage(), e);
			Thread.currentThread().interrupt(); /* Untested. Just for compliant */
		}
		seLinuxEnforceResponseValue = seLinuxEnforceAppender.toString();
		seLinuxPropResponseValue = seLinuxPropAppender.toString();
        if (isEnforcing(seLinuxEnforceResponseValue, seLinuxPropResponseValue)) {
            jsonObject.put("Status", true);
            jsonObject.put("Value", seLinuxStatus[0]);
        }
        else if (isPermissive(seLinuxEnforceResponseValue, seLinuxPropResponseValue)) {
            jsonObject.put("Status", false);
            jsonObject.put("Value", seLinuxStatus[1]);
        }
        else if (checkEmptiness(seLinuxEnforceResponseValue, seLinuxPropResponseValue)) {
            jsonObject.put("Status", true);
            jsonObject.put("Value", seLinuxStatus[0]);
        }
        else {
            jsonObject.put("Status", false);
            jsonObject.put("Unexpected value", "");
        }
		return jsonObject;
	}

	public JSONObject lookForDebugging(Context context) throws JSONException {
		JSONObject jsonObject = new JSONObject();
		boolean status = isEmulator() || isAdbActive(context);
		jsonObject.put("ActiveDebugging", status);
		return jsonObject;
	}

	private JSONArray searchUserPackages(PackageManager packageManager) throws JSONException {
		List<ApplicationInfo> applicationInfoList = packageManager
		                                                .getInstalledApplications(PackageManager.GET_META_DATA);
		ArrayList<JSONObject> packages = new ArrayList<JSONObject>();
		for (ApplicationInfo applicationInfo : applicationInfoList) {
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("Package Name", applicationInfo.packageName);
			packages.add(jsonObject);
		}
		return new JSONArray(packages);
	}

	private JSONObject searchIllegalApps(PackageManager packageManager) throws JSONException {
		JSONObject jsonObject = new JSONObject();
		List<ApplicationInfo> applicationInfoList = packageManager
		                                                .getInstalledApplications(PackageManager.GET_META_DATA);
        if (isSuBinaryPresent() || isRwMounted()) {
            jsonObject.put("Detected", true);
            return jsonObject;
        }
		for (ApplicationInfo applicationInfo : applicationInfoList) {
			for (String pkg : knownRootAppsPackages) {
				if (applicationInfo.packageName.equals(pkg)) {
					jsonObject.put("Detected", true);
					return jsonObject;
				}
			}
			for (String pkg : knownRootCloakingPackages) {
				if (applicationInfo.packageName.equals(pkg)) {
					jsonObject.put("Detected", true);
					return jsonObject;
				}
			}
		}
		jsonObject.put("Detected", false);
		return jsonObject;
	}

	private boolean isSuBinaryPresent() {
	    for (int i = 0; i < suPaths.length; i++) {
	        if (checkPresence(suPaths[i])) {
	            return true;
	        }
	    }
	    return false;
	}

	private boolean isRwMounted() {
        String[] mountedPaths;

	    mountedPaths = mountProcess();
	    if (mountedPaths == null) {
	        return false;
	    }
	    for (String path : mountedPaths) {
	        String[] pathArguments = path.split(" ");
	        if (pathArguments.length < 4) {
                Log.e("MiPass", "Error while formatting mount path");
                continue;
	        }
	        String mountPoint = pathArguments[2];
	        String mountOptions = pathArguments[5];
	        for (String checkPath : pathsThatShouldNotBeWritable) {
	            if (mountPoint.equalsIgnoreCase(checkPath)) {
	                for (String opt : mountOptions.split("[\\(\\,\\)]")) {
	                    if (opt.equalsIgnoreCase("rw")) {
	                        return true;
	                    }
	                }
	            }
	        }
	    }
	    return false;
	}

	private boolean isEmulator() {
		return (Build.BRAND.startsWith("generic") && Build.DEVICE.startsWith("generic"))
				|| Build.FINGERPRINT.startsWith("generic")
				|| Build.FINGERPRINT.startsWith("unknown")
				|| Build.HARDWARE.contains("goldfish")
				|| Build.HARDWARE.contains("ranchu")
				|| Build.MODEL.contains("google_sdk")
				|| Build.MODEL.contains("Emulator")
				|| Build.MODEL.contains("Android SDK built for x86")
				|| Build.MANUFACTURER.contains("Genymotion")
				|| Build.PRODUCT.contains("sdk_google")
				|| Build.PRODUCT.contains("google_sdk")
				|| Build.PRODUCT.contains("sdk")
				|| Build.PRODUCT.contains("sdk_x86")
				|| Build.PRODUCT.contains("vbox86p")
				|| Build.PRODUCT.contains("emulator")
				|| Build.PRODUCT.contains("simulator");
	}

	private boolean isAdbActive(Context context) {
		try {
			return (Settings.Global.getInt(context.getContentResolver(), Settings.Global.ADB_ENABLED) == 1) ||
			    (Settings.Global.getInt(context.getContentResolver(), Settings.Global.DEVELOPMENT_SETTINGS_ENABLED) == 1);
		}
		catch (Settings.SettingNotFoundException e) {
		    Log.e("MiPass", e.getMessage(), e);
			return false;
		}
	}

	private boolean isEnforcing(String seLinuxEnforceResponseValue, String seLinuxPropResponseValue) {
	    return seLinuxStatus[0].equalsIgnoreCase(seLinuxEnforceResponseValue) ||
    	    seLinuxStatus[2].equalsIgnoreCase(seLinuxPropResponseValue);
    }

    private boolean checkEmptiness(String seLinuxEnforceResponseValue, String seLinuxPropResponseValue) {
    	if ("".equalsIgnoreCase(seLinuxEnforceResponseValue) && "".equalsIgnoreCase(seLinuxPropResponseValue)) {
    	    seLinuxEnforceResponseValue = "Enforcing";
    	    seLinuxPropResponseValue = "enforcing";
            return true;
        }
        return false;
    }

    private boolean isPermissive(String seLinuxEnforceResponseValue, String seLinuxPropResponseValue) {
    	return seLinuxStatus[1].equalsIgnoreCase(seLinuxEnforceResponseValue) ||
    	    seLinuxStatus[3].equalsIgnoreCase(seLinuxPropResponseValue);
    }

    private String[] mountProcess() {
        try {
            InputStream mountStream = Runtime.getRuntime().exec("mount").getInputStream();
            if (mountStream == null) {
                return null;
            }
            String propertyValue = new Scanner(mountStream).useDelimiter("\\A").next();
            return propertyValue.split("\n");
        }
        catch (Exception e) {
            Log.e("MiPass", e.getMessage(), e);
            return null;
        }
    }

    private boolean checkPresence(String path) {
        return new File(path).exists();
    }
}
