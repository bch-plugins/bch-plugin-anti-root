var exec = require('cordova/exec');

var PLUGIN_NAME = 'rootdetector';

var rootdetector = {
    listApps: function (success, error) {
        exec(success, error, PLUGIN_NAME, 'listApps', []);
    },
    lookForRootApp: function (success, error) {
        exec(success, error, PLUGIN_NAME, 'lookForRootApp', []);
    },
    isSELinux: function (success, error) {
        exec(success, error, PLUGIN_NAME, 'isSELinux', []);
    },
    isEmulatorOrActiveDebugging: function (success, error) {
        exec(success, error, PLUGIN_NAME, 'isEmulatorOrActiveDebugging', []);
    }
};

module.exports = rootdetector;